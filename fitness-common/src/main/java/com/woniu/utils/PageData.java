package com.woniu.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分页对象模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "分页响应对象", description = "分页响应结果")
public class PageData {
    @ApiModelProperty("总记录数")
    private Long total;
    @ApiModelProperty("分页数据")
    private Object data;
    @ApiModelProperty("当前页数")
    private Long currentPage;

    public PageData(Long total, Object data) {
        this.total = total;
        this.data = data;
    }
}

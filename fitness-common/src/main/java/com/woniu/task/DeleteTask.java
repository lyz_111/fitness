package com.woniu.task;


import com.woniu.constant.RedisConstant;
import com.woniu.utils.MinioClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.Set;

/**
 *  如果需要使用spring的定时任务，使用该类
 */
//@Component
@Slf4j
public class DeleteTask {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private MinioClientUtil minioClientUtil;


//    @Scheduled(cron = "0/30 * * * * ?")
    public void deleteTask(){

        log.info("执行删除任务");
        try {
            clearImg();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void clearImg() throws Exception {

        //去除 数据库 与图片服务器 不一样图片数据
        Set difference = redisTemplate.opsForSet()
                .difference(RedisConstant.MINIO_IMG,RedisConstant.DB_IMG);


        for (Object o : difference) {
            String[] split = o.toString().split("/");
            log.info("删除图片{}",split[split.length - 1]);

            minioClientUtil.removeObject(split[split.length - 1]);
        }
    }

}

package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("appointment")
@ApiModel(value = "Appointment对象", description = "")
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("预约id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("课程id")
    private String courseId;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("场馆id")
    private String gymId;

    @ApiModelProperty("排班表id，查询预约人数及时间")
    private String scheduleId;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

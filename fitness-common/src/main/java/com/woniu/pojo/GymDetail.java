package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("gym_detail")
@ApiModel(value = "GymDetail对象", description = "")
public class GymDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("场地详情id")
    private String id;

    @ApiModelProperty("场地名称")
    private String name;

    @ApiModelProperty("场馆id")
    private String gymId;

    @ApiModelProperty("场地默认容纳人数")
    private Integer numbers;

    @ApiModelProperty("场地用途，1为团课，2,为私教课")
    private Integer purposes;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

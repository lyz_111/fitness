package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("syslog")
public class Syslog implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String username;
    private String operation;
    private String method;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}

package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("gym")
@ApiModel(value = "Gym对象", description = "")
public class Gym implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("场馆id")
  private String id;

  @ApiModelProperty("场馆名称")
  private String name;

  @ApiModelProperty("场馆电话")
  private String telephone;

  @ApiModelProperty("场馆地址")
  private String address;

  @ApiModelProperty("场馆邮箱")
  private String email;

  @ApiModelProperty("排序")
  private Integer sort;

  @ApiModelProperty("状态，1为正常，2为非正常")
  private String status;

  @ApiModelProperty("场馆图片")
  private String photo;

  @ApiModelProperty("场馆简介")
  private String introduce;

  @ApiModelProperty("默认场馆(当用户无法定位时，系统默认推送显示默认场馆给到用户，默认场馆只允许一个)")
  private String tacit;

  @ApiModelProperty("更新时间")
  @TableField(fill = FieldFill.INSERT_UPDATE)
  private Date updateTime;

  @ApiModelProperty("创建时间")
  @TableField(fill = FieldFill.INSERT)
  private Date createTime;


}

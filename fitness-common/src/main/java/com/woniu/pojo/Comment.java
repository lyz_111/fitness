package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-10
 */
@Data
@TableName("comment")
@ApiModel(value = "评论对象", description = "")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("评论id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("被评论对象id")
    private String categoryId;

    @ApiModelProperty("被评论对象类别(0,课程.1,教练.2,商品)")
    private String category;

    @ApiModelProperty("评论内容")
    private String content;

    @ApiModelProperty("评论星级")
    private String level;

    @ApiModelProperty("评论人id")
    private String userId;
    @ApiModelProperty("更改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

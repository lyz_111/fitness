package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
  @TableName("user_address")
@ApiModel(value = "UserAddress对象", description = "")
public class UserAddress implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("地址表id")
      private String id;


      @ApiModelProperty("用户id")
      private String userId;


      @ApiModelProperty("是否是默认地址，1是普通地址，2是默认地址")
      private int acquiesce;


      @ApiModelProperty("收货地址信息")
      private String addressInfo;

      @ApiModelProperty("收货手机号码")
      private String telephone;

      @ApiModelProperty("收货人")
      private String receiver;

      @ApiModelProperty("物流信息，可以存储物流单号")
      private String logisticsInfo;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;


}

package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-08
 */
@Getter
@Setter
@TableName("course_detail")
@ApiModel(value = "CourseDetail对象", description = "")
public class CourseDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("课程详情表")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("课程id")
    private String courseId;

    @ApiModelProperty("课程介绍")
    private String introduce;

    @ApiModelProperty("工具训练，课程所需要的工具")
    private String toolsDesc;

    @ApiModelProperty("运动康复")
    private String rehabilitation;

    @ApiModelProperty("功能性伸展课程")
    private String functionality;


}

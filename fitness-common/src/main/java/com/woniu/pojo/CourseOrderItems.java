package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("course_order_items")
@ApiModel(value = "CourseOrderItems对象", description = "")
public class CourseOrderItems implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("订单项id")
        private String id;

      @ApiModelProperty("排班课程id")
      private String scheduleId;

      @ApiModelProperty("课程订单id")
      private String courseOrderId;

      @ApiModelProperty("商品名称")
      private String name;

      @ApiModelProperty("商品单价")
      private BigDecimal price;

      @ApiModelProperty("商品数量")
      private Integer numbers;

      @ApiModelProperty("订单项总价格")
      private BigDecimal totalAmount;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;


}

package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("course_order")
@ApiModel(value = "CourseOrder对象", description = "")
public class CourseOrder implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("课程订单id")
        private String id;

      @ApiModelProperty("订单总金额")
      private String totalAmount;

      @ApiModelProperty("付款方式，1为微信，2为支付宝，3为现金，4为银行卡，5为其他")
      private Integer paymentMethod;

      @ApiModelProperty("用户id，根据id查询客户名称")
      private String userId;

      @ApiModelProperty("支付日期")
      private Date paymentTime;

      @ApiModelProperty("发货时间")
      private Date deliveryTime;

      @ApiModelProperty("用户地址id，用户查询收货地址")
      private String userAddressId;

      @ApiModelProperty("物流信息，存放物流单号")
      private String logisticsInfo;

      @ApiModelProperty("备注信息")
      private String remarks;

      @ApiModelProperty("订单类型，1为实物订单")
      private Integer orderType;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;


}

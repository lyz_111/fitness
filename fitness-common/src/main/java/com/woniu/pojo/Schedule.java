package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("schedule")
@ApiModel(value = "Schedule对象", description = "")
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("日期表id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("课程表id")
    private String courseId;

    @ApiModelProperty("场馆id")
    private String gymId;

    @ApiModelProperty("场地id")
    private String gymDetailId;

    @ApiModelProperty("课程时间安排")
    private String courseTime;

    @ApiModelProperty("星期几，1为星期一，依次类推")
    private Integer dayOfWeek;

    @ApiModelProperty("具体排班日期，如2023/7/3")
    private Date date;

    @ApiModelProperty("预约人数")
    private Integer numberOfAppointments;

    @ApiModelProperty("实际到场人数")
    private Integer actualNumber;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("goods_carts")
@ApiModel(value = "GoodsCarts对象", description = "")
public class GoodsCarts implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("购物车id")
    private String id;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("商品id")
    private String goodsId;

    @ApiModelProperty("自增排序,新增的序号最大")
    private Integer sort;

    @ApiModelProperty("商品图片")
    private String goodsPhoto;

    @ApiModelProperty("商品名字")
    private String goodsName;

    @ApiModelProperty("商品价格")
    private BigDecimal price;

    @ApiModelProperty("购买数量")
    private Integer numbers;

    @ApiModelProperty("商品总价")
    private BigDecimal totalAmount;

    @ApiModelProperty("是否删除")
    private Integer isDelete;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

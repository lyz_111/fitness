package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Getter
@Setter
  @TableName("goods_type")
@ApiModel(value = "GoodsType对象", description = "")
public class GoodsType implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("商品类型id")
        private String id;

      @ApiModelProperty("商品类型")
      private String type;


}

package com.woniu.pojo;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Getter
@Setter
  @TableName("goods")
@ApiModel(value = "Goods对象", description = "")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("商品id")
        private String id;

      @ApiModelProperty("商品编号")
      private Integer numbers;

      @ApiModelProperty("商品分类id")
      private String goodsSortId;

      @ApiModelProperty("商品类型id")
      private String goodsTypeId;

      @ApiModelProperty("商品名称")
      private String name;

      @ApiModelProperty("商品价格")
      private BigDecimal price;

      @ApiModelProperty("商品照片")
      private String photo;

      @ApiModelProperty("商品介绍")
      private String introduce;

      @ApiModelProperty("商品视频")
      private String video;

      @ApiModelProperty("商品标签")
      private String sign;

      @ApiModelProperty("销量")
      private Integer sales;

      @ApiModelProperty("库存")
      private Integer store;

      @ApiModelProperty("逻辑删除，0是未删除，1是删除")
      @TableLogic
      private Integer isDelete;

      @ApiModelProperty("是否上线，1上线，2未上线")
      private Integer isOnline;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;

}

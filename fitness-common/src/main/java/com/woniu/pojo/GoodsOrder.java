package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("goods_order")
@ApiModel(value = "GoodsOrder对象", description = "")
public class GoodsOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商品订单表id")
    private String id;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("订单状态，1未付款，2已付款，3已取消")
    private Integer status;

    @ApiModelProperty("订单类型，1实物订单，2虚拟订单，3积分订单")
    private Integer type;

    @ApiModelProperty("订单总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

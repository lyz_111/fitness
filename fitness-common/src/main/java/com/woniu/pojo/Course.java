package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Data
@TableName("course")
@ApiModel(value = "Course对象", description = "")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("课程id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("场馆id")
    private String gymId;

    @ApiModelProperty("课程名称")
    private String name;

    @ApiModelProperty("课程价格")
    private BigDecimal price;

    @ApiModelProperty("课程描述")
    private String details;

    @ApiModelProperty("课程图片")
    private String photo;

    @ApiModelProperty("教练id")
    private String coachId;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("课程时长")
    private Integer duration;

    @ApiModelProperty("可预约人数")
    private Integer orderNumber;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}

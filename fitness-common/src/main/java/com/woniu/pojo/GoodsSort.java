package com.woniu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Getter
@Setter
  @TableName("goods_sort")
@ApiModel(value = "GoodsSort对象", description = "")
public class GoodsSort implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("商品分类表")
        private String id;

      @ApiModelProperty("分类名称")
      private String sortName;

      @ApiModelProperty("分类编号")
      private Integer numbers;

      @ApiModelProperty("状态，1为开启，2为关闭，默认为1")
      private Integer status;

      @ApiModelProperty("更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;


}

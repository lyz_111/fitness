package com.woniu.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
@ApiModel("分页查询条件")
public class GoodsSortDTO {
    @ApiModelProperty("商品分类id")
    private String id;

    @ApiModelProperty("商品名称")
    private String sortName;

    @ApiModelProperty("分类编号")
    private Integer numbers;

    @ApiModelProperty("状态，1为开启，2为关闭，默认为1")
    private Integer status;

    private Date updatetime;
}

package com.woniu.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

@Data
@ApiModel("商品分页查询条件")
public class GoodsDTO implements Comparable<GoodsDTO> {

    private String id;
    @ApiModelProperty("商品名称")
    private String name;
    @ApiModelProperty("商品分类id")
    private Integer goodsSortId;
    @ApiModelProperty("商品类型id")
    private Integer goodsTypeId;
    @ApiModelProperty("商品编号")
    private Integer numbers;
    @ApiModelProperty("商品分类名称")
    private String sortName;
    @ApiModelProperty("商品类型")
    private String type;
    @ApiModelProperty("商品图片")
    private String photo;
    @ApiModelProperty("商品价格")
    private BigDecimal price;
    @ApiModelProperty("是否上线")
    private Integer isOnline;
    @ApiModelProperty("商品标签")
    private String sign;
    @ApiModelProperty("商品销量")
    private Integer sales;
    @ApiModelProperty("查询字段:名字和编号")
    private String nameOrNumbers;

    //自然排序,比较器
    @Override
    public int compareTo(@NotNull GoodsDTO o) {
        return this.getNumbers() - o.getNumbers();
    }
}

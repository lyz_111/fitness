package com.woniu.model.dto;


import lombok.Data;

@Data
public class UserRegisterDTO {

    private String username;
    private String password;
    private String rePassword;
    private String code;
}

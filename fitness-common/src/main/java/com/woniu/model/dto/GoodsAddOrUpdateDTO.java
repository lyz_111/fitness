package com.woniu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GoodsAddOrUpdateDTO {
    @ApiModelProperty("商品id")
    private String id;
    @ApiModelProperty("商品类型Id")
    private String goodsTypeId;
    @ApiModelProperty("商品分类Id")
    private String goodsSortId;
    @ApiModelProperty("商品分类")
    private String sortName;
    @ApiModelProperty("商品类型")
    private String type;
    @ApiModelProperty("商品名称")
    private String name;
    @ApiModelProperty("商品价格")
    private BigDecimal price;
    @ApiModelProperty("商品介绍")
    private String introduce;
    @ApiModelProperty("标签")
    private String sign;
    @ApiModelProperty("库存")
    private String store;
    @ApiModelProperty("商品头图")
    private String photo;
    @ApiModelProperty("主图视频")
    private String video;
}

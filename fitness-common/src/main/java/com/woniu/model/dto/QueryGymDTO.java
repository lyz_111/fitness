package com.woniu.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QueryGymDTO {
  @ApiModelProperty("场馆名称")
  private String name;
  @ApiModelProperty("场馆地址")
  private String address;
  @ApiModelProperty("状态，1为正常，2为非正常")
  private String status;
  @ApiModelProperty("排序")
  private Integer sort;
}

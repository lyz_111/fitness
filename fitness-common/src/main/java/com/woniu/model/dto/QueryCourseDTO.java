package com.woniu.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("封装查询课程对象")
public class QueryCourseDTO {

    @ApiModelProperty("课程名称")
    private String courseName;
    @ApiModelProperty("教练id")
    private String coachId;

}
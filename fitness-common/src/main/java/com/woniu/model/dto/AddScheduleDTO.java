package com.woniu.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.TimeZone;

@Data
@ApiModel("添加预约对象")
public class AddScheduleDTO {
    @ApiModelProperty("预约id")
    private String id;
    @ApiModelProperty("课程id")
    private String courseId;
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("场馆id")
    private String gymId;
    @ApiModelProperty("场地id")
    private String gymDetailId;
    @ApiModelProperty("具体排班日期")
    private Date date;
    @ApiModelProperty("预约星期几")
    private Integer dayOfWeek;
    @ApiModelProperty("预约课程时间")
    private String courseTime;
    @ApiModelProperty("预约人数")
    private Integer numberOfAppointments;
}

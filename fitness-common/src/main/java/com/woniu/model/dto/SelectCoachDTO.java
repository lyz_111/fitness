package com.woniu.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel("搜索教练对象")
public class SelectCoachDTO {
    @ApiModelProperty("所在场馆")
    private String gymId;
    @ApiModelProperty("模糊查询字段")
    private String name;

}

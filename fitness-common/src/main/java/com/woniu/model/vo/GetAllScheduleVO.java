package com.woniu.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
@ApiModel("展示预约对象")
public class GetAllScheduleVO {
    @ApiModelProperty("预约id")
    private String id;
    @ApiModelProperty("对应的课程(通过courseid查)")
    private String courseName;
    @ApiModelProperty("使用的场馆(通过gymid查)")
    private String gymName;
    @ApiModelProperty("使用的场地(通过gym_detail_id查)")
    private String gymDetailName;
    @ApiModelProperty("课程教练")
    private String coachName;
    @ApiModelProperty("预约课程时间")
    private String courseTime;
    @ApiModelProperty("预约星期几")
    private Integer dayOfWeek;
    @ApiModelProperty("具体预约日期")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date date;
    @ApiModelProperty("预约人数")
    private Integer numberOfAppointments;
    @ApiModelProperty("实际到场人数")
    private Integer actualNumber;
}

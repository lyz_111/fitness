package com.woniu.model.vo;

import lombok.Data;

@Data
public class GoodsSortVo {
    private String id;
    private String sortName;
    private Integer numbers;
    private Integer status;
}

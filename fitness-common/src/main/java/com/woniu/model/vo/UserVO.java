package com.woniu.model.vo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

@Data
public class UserVO {

    @ApiModelProperty("用户id")
    private String id;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("收货人")
    private String receiver;
    @ApiModelProperty("收货地址信息")
    private String addressInfo;
    @ApiModelProperty("收货手机号码")
    private String telephone;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("用户身份，0是普通用户，1是管理员")
    private Integer power;

}

package com.woniu.model.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@ApiModel(value = "Coach展示对象", description = "")
public class CoachVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("教练id")
    private String id;

    @ApiModelProperty("教练姓名")
    private String name;

    @ApiModelProperty("教练性别")
    private String gender;

    @ApiModelProperty("教练角色，1为普通教练，2为店长")
    private Integer role;

    @ApiModelProperty("教练头像")
    private String photo;

    @ApiModelProperty("教练电话")
    private String telephone;

    @ApiModelProperty("教练简介")
    private String introduce;

    @ApiModelProperty("教练签名")
    private String sign;

    @ApiModelProperty("工作年限")
    private Integer employmentTime;

    @ApiModelProperty("擅长领域")
    private String prominent;

    @ApiModelProperty("每日私教课上限")
    private Integer max;

    @ApiModelProperty("所在场馆")
    private String gymId;

    @ApiModelProperty("逻辑删除，0为正常，1为删除")
    private Integer isDelete;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("所属场馆")
    private String gymName;

}

package com.woniu.model.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("comment")
@ApiModel(value = "评论对象", description = "")
public class CommentVO {

    @ApiModelProperty("评论id")
    private String id;

    @ApiModelProperty("评论id")
    private String userId;

    @ApiModelProperty("评论人姓名")
    private String username;


    @ApiModelProperty("被评论id")
    private String categoryId;

    @ApiModelProperty("被评论对象类别(0,课程.1,教练.2,商品)")
    private String category;

    @ApiModelProperty("评论内容")
    private String content;

    @ApiModelProperty("评论星级")
    private String level;


    @ApiModelProperty("更改时间")
     private Date updateTime;


}

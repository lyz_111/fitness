package com.woniu.globalExceptionHandler;


import com.woniu.exception.LoginException;
import com.woniu.interceptor.LoginInterceptor;
import com.woniu.utils.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = LoginException.class)
    public Result loginHandler(){
        // 捕获到非法登录
        System.out.println("捕获到非法登录");
        return new Result(401,"登录非法");
    }
}

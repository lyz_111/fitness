package com.woniu.config;

import com.woniu.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Interceptor implements WebMvcConfigurer {


    @Bean
    public LoginInterceptor LoginHandlerInterceptor(){
        return new LoginInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {


        registry.addInterceptor(LoginHandlerInterceptor())
                //拦截所有请求
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/code",//验证码
                        "/file/upload",//文件上传
                        "/user/login",
                        "/user/register",
                        "/user/info",
                        "/user/register",
                        "/**/*.js",
                        "/**/*.css",
                        "/**/*.ico",
                        "/**/*.html",
                        "/**/*.jpg",
                        "/**/*.png",
                        "/error",
                        "/**/*.min.map",
                        "/swagger-resources",
                        "/v3/api-docs",
                        "/v2/api-docs",
                        "/js/**",
                        "/css/**",
                        "/images/**",
                        "/img/**",
                        "/plugins/**"
                );
    }
}

package com.woniu.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 解决后端的跨域问题:
 *
 * 1、解决方案1：配置拦截器、过滤器
 * 2、使用注解    @CrossOrigin
 */

@Configuration // 一定不要忽略此注解
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 所有接口

                .allowCredentials(true) // 是否发送 Cookie
                .allowedOrigins("*") // 支持域
                //需要支持OPTION,来解决跨越携带token问题
                .allowedMethods(new String[]{"GET", "POST", "PUT", "DELETE","OPTION"}) // 支持方法
                .allowedHeaders("*")
                .exposedHeaders("*");
    }
}

package com.woniu.constant;

/**
 * redis key常量
 */
public class RedisConstant {

    //redis中存储 minio的图片的key
    public final static String MINIO_IMG = "groupOne:db:minioImg";

    //redis中存储 数据库的图片的key
    public final static String DB_IMG = "groupOne:db:img";

    //redis中存储 登录和注册验证码的key
    public final static String LOGIN_AND_REGISTER_CODE = "groupOne:db:login:code";

    //redis中存储 用户登录信息的key
    public final static String USER_INFO = "groupOne:db:user:info";


}

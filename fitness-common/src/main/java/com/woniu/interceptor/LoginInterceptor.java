package com.woniu.interceptor;


import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.woniu.constant.RedisConstant;
import com.woniu.exception.LoginException;
import lombok.SneakyThrows;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * 自定义登录拦截器，拦截非法登录
 */
public class LoginInterceptor implements HandlerInterceptor {


    @Resource
    private RedisTemplate redisTemplate;

    //在执行目标方法前拦截
    @SneakyThrows
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //配置此代码解决 拦截器不处理option请求问题，如果不放行，token不会携带，
        //原因是 浏览器先发一个option请求，再发真正的请求
        if(request.getMethod().equals(RequestMethod.OPTIONS.name()))
        {
            response.setHeader("Access-Control-Allow-Origin","*");
            response.setHeader("Access-Control-Allow-Headers","*");
            response.setHeader("Access-Control-Allow-Methods","*");
            response.setHeader("Access-Control-Allow-Credentials","true");
            response.setHeader("Access-Control-Max-Age","3600");
            response.setStatus(HttpStatus.OK.value());

            return true;
        }

        String token = request.getHeader("token");


        // 判断token是否为空
        if (token == null) {
            throw new LoginException();
        }

        // 去查询redis验证token是否过期
        JWT jwt = JWTUtil.parseToken(token);

        String username = (String) jwt.getPayload("username");



        Object obj = redisTemplate.opsForValue().get(RedisConstant.USER_INFO + ":" + username);


        if (obj == null) {
            throw new LoginException();
        }

        //刷新redis中的token时间为30分钟
        redisTemplate.opsForValue().set(RedisConstant.USER_INFO + ":" + username, token, 30, TimeUnit.MINUTES);

        return true;
    }
}

package com.woniu;

import com.woniu.pojo.Usercount;
import com.woniu.service.IUsercountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;

@Component
public class CountUserTask {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IUsercountService usercountService;
    @Scheduled(cron = "50 59 23 * * ?")
    //每天23:59:50执行统计操作
    public void getCountTask(){
        Set persons = redisTemplate.opsForSet().members("persons");
        Usercount usercount=new Usercount(new Date(),persons.size());
        usercountService.save(usercount);
        redisTemplate.delete("persons");
    }
}

package com.woniu.controller;

import com.woniu.model.dto.QueryCourseDTO;
import com.woniu.pojo.Course;
import com.woniu.service.ICourseService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/course")
@Api(tags = "课程控制层")
public class CourseController extends BaseController{

    @Autowired(required = false)
    private ICourseService courseService;

    //分页查询课程、根据课程名称及教练名称模糊查寻课程
    @PostMapping("/page/{currentPage}/{pageSize}")
    @ApiOperation("分页模糊查")
    public Result getCoursePage(
            @PathVariable Integer currentPage,
            @PathVariable Integer pageSize,
            @RequestBody QueryCourseDTO queryCourseDTO){
        PageData pageData = courseService.getCoursePage(currentPage, pageSize, queryCourseDTO);
        return toDataResult(pageData);
    }

    //添加、修改
    @PostMapping
    @ApiOperation("添加或者修改")
    public Result saveOrUpdateCourse(@RequestBody Course course){
        String courseId = courseService.saveOrUpdateCourse(course);
        return toDataResult(courseId);
    }

    //根据id删
    @DeleteMapping("/{courseId}")
    @ApiOperation("根据id删")
    public Result deleteCourseById(@PathVariable String courseId){
        boolean flag = courseService.removeById(courseId);
        return toResult(flag);
    }

    //根据id查
    @GetMapping("/{courseId}")
    @ApiOperation("根据id查")
    public Result getCourseById(@PathVariable String courseId){
        Course course = courseService.getById(courseId);
        return toDataResult(course);
    }
    @GetMapping("/all")
    public Result getAll(){
        List<Course> list = courseService.list();
        return toDataResult(list);
    }

}

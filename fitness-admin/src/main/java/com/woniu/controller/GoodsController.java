package com.woniu.controller;

import com.woniu.model.dto.GoodsAddOrUpdateDTO;
import com.woniu.model.dto.GoodsDTO;
import com.woniu.pojo.Goods;
import com.woniu.service.IGoodsService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/goods")
@Api(tags = "商品列表")
public class GoodsController extends BaseController{
    @Resource
    private IGoodsService goodsService;

    @ApiOperation("查询所有商品")
    @GetMapping("/all")
    public Result queryGoods(){
        List<Goods> list = goodsService.list();
        return toDataResult(list);
    }

    @ApiOperation("分页+搜索的查询显示")
    @PostMapping("/{pageNum}/{pageSize}")
    public Result queryGoodsPage(@PathVariable("pageNum") Integer pageNum,
                                 @PathVariable("pageSize") Integer pageSize,
                                 @RequestBody GoodsDTO goodsDTO){
        PageData pageData = goodsService.queryGoodsPage(pageNum,pageSize,goodsDTO);
        return toDataResult(pageData);
    }

    @ApiOperation("添加商品")
    @PostMapping()
    public Result addGoods(@RequestBody GoodsAddOrUpdateDTO goodsAddDTO){

        System.out.println("goodsAddDTO = " + goodsAddDTO);
        int flag = goodsService.addGoods(goodsAddDTO);
        return toResult(flag);
    }

    @ApiOperation("Id查询商品")
    @GetMapping("/{id}")
    public Result queryGoodsId(@PathVariable("id") String id){
        Goods goods = goodsService.getById(id);
        return toDataResult(goods);
    }

    @ApiOperation("查询上线商品")
    @GetMapping("/onLine")
    public Result queryOnline() {
        int flag = goodsService.queryOnline();
        return toDataResult(flag);
    }

    @ApiOperation("查询未上线商品")
    @GetMapping("/no/onLine")
    public Result queryNoOnline() {
        return toDataResult(goodsService.queryNoOnline());
    }

    @ApiOperation("删除商品")
    @DeleteMapping("/{id}")
    public Result deleteGoodsId(@PathVariable("id") String id){
        boolean flag = goodsService.removeById(id);
        return toResult(flag);
    }

    @ApiOperation("编辑商品")
    @PutMapping()
    public Result updateGoods(@RequestBody GoodsAddOrUpdateDTO goodsUpdateDTO){
        int flag = goodsService.updateGoods(goodsUpdateDTO);
        return toResult(flag);
    }

    @ApiOperation("批量删除商品")
    @DeleteMapping("/")
    public Result deleteAllGoods(@RequestBody List<String> ids){
        boolean flag = goodsService.removeBatchByIds(ids);
        return toResult(flag);
    }
}

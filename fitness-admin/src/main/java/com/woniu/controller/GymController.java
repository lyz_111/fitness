package com.woniu.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.woniu.model.dto.QueryGymDTO;
import com.woniu.pojo.Gym;
import com.woniu.service.IGymService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/gym")
@Api(tags = "场馆管理类")
public class GymController extends BaseController {

  @Autowired
  IGymService gymService;

  @ApiOperation("模糊查询+分页")
  @PostMapping("/{currentPage}/{pageSize}")
  public Result findGymPage(
      @PathVariable Integer currentPage,
      @PathVariable Integer pageSize,
      @RequestBody QueryGymDTO queryGymDTO
  ) {
    PageData page = gymService.findGymPage(currentPage, pageSize, queryGymDTO);
    return toDataResult(page);
  }

  @ApiOperation("显示根据id")
  @GetMapping("/{id}")
  public Result findAllGym(@PathVariable String id) {
    Gym gym = gymService.getById(id);
    return toDataResult(gym);
  }
  @ApiOperation("显示所有场馆")
  @GetMapping("/all")
  public Result findAllGym() {
    List<Gym> list = gymService.list();
    return toDataResult(list);
  }

  @ApiOperation("添加|修改场馆")
  @PostMapping()
  public Result addOrUpdateGym(@RequestBody Gym gym) {
    boolean flag = gymService.saveOrUpdate(gym);
    return toDataResult(gym.getId());
  }

  @ApiOperation("单个删除")
  @DeleteMapping("/{id}")
  public Result deleteGymById(@PathVariable String id) {
    boolean flag = gymService.deleteGymById(id);
    return toDataResult(flag);
  }

  @ApiOperation("多个删除")
  @DeleteMapping("/delBatch")
  public Result deleteGyms(@RequestBody List<String> ids) {
    boolean flag = gymService.removeBatchByIds(ids);
    return toDataResult(flag);
  }

  @GetMapping("/byStatus")
  public Result getGymByStatus() {
    List<Gym> gymList = gymService.getGymByStatus();
    return toDataResult(gymList);
  }

}

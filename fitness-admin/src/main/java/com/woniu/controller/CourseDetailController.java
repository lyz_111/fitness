package com.woniu.controller;

import com.woniu.pojo.CourseDetail;
import com.woniu.service.ICourseDetailService;
import com.woniu.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-08
 */
@RestController
@RequestMapping("/courseDetail")
public class CourseDetailController extends BaseController{

    @Autowired(required = false)
    private ICourseDetailService courseDetailService;

    //通过courseId查询课程细节描述
    @GetMapping("/{courseId}")
    public Result getCourseDetailByCourseId(@PathVariable String courseId){
        CourseDetail courseDetail = courseDetailService.getCourseDetailByCourseId(courseId);
        return toDataResult(courseDetail);
    }

    //添加、修改课程描述（返回courseId）
    @PostMapping
    public Result saveOrUpdateCourseDetail(@RequestBody CourseDetail courseDetail){
        String courseId = courseDetailService.saveOrUpdateCourseDetail(courseDetail);
        return toDataResult(courseId);
    }

}

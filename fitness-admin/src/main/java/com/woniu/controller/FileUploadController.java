package com.woniu.controller;



import com.woniu.constant.RedisConstant;
import com.woniu.utils.MinioClientUtil;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

@Api(tags = "文件上传")
@RestController
@RequestMapping("file")
public class FileUploadController {

    //MultipartFile接受文件格式
    //@RequestPart 可以在knife4j测试

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private MinioClientUtil minioClientUtil;

    @PostMapping("upload")
    public Result imgUpload(@RequestPart MultipartFile file) throws Exception {


        //上传的文件名
        String name = file.getOriginalFilename();
        System.out.println("name = " + name);
        //文件输入流
        InputStream in = file.getInputStream();

        //构建唯一id文件
        String fileName = UUID.randomUUID().toString() + name;
        System.out.println("imgPath = " + fileName);


        boolean flag = minioClientUtil.putObject(fileName,
                in);
        //返回存储位置
        String path = "http://121.43.63.218:9000/fitness/" + fileName;

        //上传的图片地址，放入redis
        redisTemplate.opsForSet().add(RedisConstant.MINIO_IMG,path);

        return flag ? new Result(200,"成功",path) : new Result(200,"失败",null);

    }
}

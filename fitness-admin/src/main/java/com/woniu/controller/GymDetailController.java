package com.woniu.controller;

import com.woniu.pojo.GymDetail;
import com.woniu.service.IGymDetailService;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/gymDetail")
@Api(tags = "场地管理类")
public class GymDetailController extends BaseController {

  @Autowired
  IGymDetailService gymDetailService;

  @PostMapping("/modify")
  @ApiOperation("新增|修改场地")
  public Result addOrUpdateGymPlace(@RequestBody GymDetail gymDetail) {
    boolean flag = gymDetailService.saveOrUpdate(gymDetail);
    return toResult(flag);
  }

  @GetMapping("/{gymId}")
  @ApiOperation("根据场馆id查询")
  public Result findGymPlaceByGymId(@PathVariable String gymId) {
    List<GymDetail> list = gymDetailService.findGymPlaceByGymId(gymId);
    return toDataResult(list);
  }
  @GetMapping("/show/{id}")
  @ApiOperation("根据场地id查询")
  public Result findGymPlaceById(@PathVariable String id) {
    GymDetail gymDetail = gymDetailService.getById(id);
    return toDataResult(gymDetail);
  }

  @DeleteMapping("/{id}")
  @ApiOperation("根据场所id删除")
  public Result deleteGymPlace(@PathVariable String id) {
    boolean b = gymDetailService.removeById(id);
    return toResult(b);
  }
}

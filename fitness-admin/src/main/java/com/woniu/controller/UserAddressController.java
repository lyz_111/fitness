package com.woniu.controller;

import com.woniu.pojo.UserAddress;
import com.woniu.service.IUserAddressService;
import com.woniu.utils.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/userAddress")
public class UserAddressController extends BaseController{
@Autowired
private IUserAddressService iUserService;


    @ApiOperation("增加地址")
    @PostMapping("/add")
    public Result login(@RequestBody UserAddress ua){
      return toResult(iUserService.saveOrUpdate(ua));
    }
}

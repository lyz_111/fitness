package com.woniu.controller;

import com.woniu.pojo.Usercount;
import com.woniu.service.IUsercountService;
import com.woniu.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.management.Query;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-13
 */
@RestController
@RequestMapping("/usercount")
public class UsercountController extends BaseController{
    @Autowired
    private IUsercountService usercountService;
    @GetMapping
    public Result getAll(){
        List<Usercount> list = usercountService.getAll();
        return toDataResult(list);
    }

}

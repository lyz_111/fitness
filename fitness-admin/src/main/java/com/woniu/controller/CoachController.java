package com.woniu.controller;

import com.woniu.model.dto.SelectCoachDTO;
import com.woniu.pojo.Coach;
import com.woniu.service.ICoachService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 健身教练控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/coach")
@Api(tags = "教练控制器")
public class CoachController extends BaseController {
    @Autowired
    private ICoachService ics;

    @ApiOperation("分页+搜索的查询显示")
    @PostMapping("/{currentPage}/{pageSize}")
    public Result pageAll(@PathVariable Integer currentPage,
                          @PathVariable Integer pageSize,
                          @RequestBody SelectCoachDTO scd) {
        PageData pageData = ics.pageSelect(currentPage, pageSize, scd);
        return toDataResult(pageData);
    }

    @ApiOperation("添加或修改教练方法")
    @PostMapping()
    public Result addOrUpdateCoach(@RequestBody Coach coach) {
        return toResult(ics.saveOrUpdate(coach));
    }

    @ApiOperation("删除教练方法")
    @DeleteMapping("/{id}")
    public Result deleteCoach(@PathVariable String id) {
        return toResult(ics.removeById(id));
    }

    @ApiOperation("查所有教练")
    @GetMapping("/all")
    public Result pageAll() {
        List<Coach> list = ics.list();
        return toDataResult(list);
    }

    @ApiOperation("id查个人")
    @GetMapping("/{id}")
    public Result getOne(@PathVariable String id) {
        Coach byId = ics.getById(id);
        return toDataResult(byId);
    }

    //查询指定场馆教练
    @GetMapping("/gym/{gymId}")
    @ApiOperation("根据场馆id查询教练")
    public Result getCoachByGymId(@PathVariable String gymId){
        List<Coach> coaches = ics.getCoachByGymId(gymId);
        return toDataResult(coaches);
    }

    //查询启用教练
    @GetMapping("/byOnline")
    @ApiOperation("查询正常启用教练")
    public Result getCoachByOnline(){
        List<Coach> coaches = ics.getCoachByIsOnline();
        return toDataResult(coaches);
    }

    //查询正常场馆下正常启用教练
    @GetMapping("/normal")
    @ApiOperation("查询正常场馆下正常启用教练")
    public Result getCoach(){
        List<Coach> coaches = ics.getCoach();
        return toDataResult(coaches);
    }

}

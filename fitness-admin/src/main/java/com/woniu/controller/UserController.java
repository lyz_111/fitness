package com.woniu.controller;

import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.woniu.constant.RedisConstant;
import com.woniu.model.dto.SelectUserDTO;
import com.woniu.model.dto.UserRegisterDTO;
import com.woniu.pojo.User;
import com.woniu.pojo.UserAddress;
import com.woniu.service.IUserAddressService;
import com.woniu.service.IUserService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 前端控制器
 * 用户模块  包括登录/注册等
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Api(tags = "用户控制器")
public class UserController extends BaseController {
    @Autowired
    private IUserAddressService ias;

    @Resource
    private IUserService userService;

    @Resource
    private RedisTemplate redisTemplate;

    @ApiOperation("模拟登录")
    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> params) {
        String username = params.get("username");
        String password = params.get("password");
        System.out.println("password = " + password);
        System.out.println("username = " + username);
        //验证码
        String code = params.get("code");


        //从Redis缓存中拿到Kaptcha生成的验证码，验证用户是否输入正确
        String codeText = (String) redisTemplate.opsForValue().get(RedisConstant.LOGIN_AND_REGISTER_CODE);


        //验证验证码是否失效
        if (codeText == null) {
            return unAuthorizedResult("验证码失效");
        }
        //验证验证码是否输入正确
        if (!codeText.equalsIgnoreCase(code)) {
            return unAuthorizedResult("验证码输入不正确");
        }

        //验证用户名与密码
        boolean flag = userService.login(username, password);

        //用户名与密码正确
        if (!flag) {
            return unAuthorizedResult("账号密码错误");
        }

        //验证通过，发放token
        HashMap<String, Object> hashMap = new HashMap<>();
        //1.存放用户名
        hashMap.put("username", username);

        //2.创建token,并将用户信息存入token
        String token = JWTUtil.createToken(hashMap, "groupOne".getBytes());
        //3.集合加入token
        hashMap.put("token", token);

        //3.将token保存到redis中，设置30分钟有效期
        redisTemplate.opsForValue().set(RedisConstant.USER_INFO + ":" + username, token, 30, TimeUnit.MINUTES);

        //返回集合信息
        return customMessageResult("登录成功", hashMap);

    }

    @ApiOperation("获取用户信息")
    @GetMapping("/info")
    public Result info(HttpServletRequest request) {

        //得到token
        String token = request.getHeader("token");

        Map<String, Object> data = new HashMap<>();
        //得到token中的username
        JWT jwt = JWTUtil.parseToken(token);
        String username = (String) jwt.getPayload("username");

        //根据username查询用户信息，返回头像
        String photo = userService.getPhotoByUsername(username);
        data.put("name", username);
        data.put("avatar", photo);

        return toDataResult(data);
    }


    @ApiOperation("退出登录")
    @PostMapping("/logout")
    public Result logout(HttpServletRequest request) {
        //获取用户名
        String username = userService.getUsername();
        //得到rides中设置的key
        String delKey = RedisConstant.USER_INFO + ":" + username;
        Boolean delete = redisTemplate.delete(delKey);

        return delete ? unAuthorizedResult("退出成功") : unAuthorizedResult("服务器繁忙，请稍后重试");
    }


    @ApiOperation("用户注册")
    @PostMapping("/register")
    public Result register(@RequestBody UserRegisterDTO userRegisterDTO) {
        //获取用户名,密码，二次确认密码，验证码
        String username = userRegisterDTO.getUsername();
        String password = userRegisterDTO.getPassword();
        String rePassword = userRegisterDTO.getRePassword();
        String code = userRegisterDTO.getCode();

        User user = userService.getUserByUserName(username);

        // 判断用户名是否存在
        if (user != null) {
            return unAuthorizedResult("用户名已存在");
        }
        // 判断密码是否有效
        if (password == null && rePassword == null) {
            return unAuthorizedResult("无效密码");
        }
        // 判断两次密码是否一致
        if (!password.equals(rePassword)) {
            return unAuthorizedResult("两次密码不一致");
        }
        //得到redis中保存的验证码
        String codeText = (String) redisTemplate.opsForValue().get(RedisConstant.LOGIN_AND_REGISTER_CODE);
        //判断验证码是否输入正确
        if (!codeText.equalsIgnoreCase(code)) {
            return unAuthorizedResult("验证码不正确");
        }

        //加密密码
        String BCryptPassword = BCrypt.hashpw(password);

        //构建User对象
        User user1 = new User(null, username, BCryptPassword);
        // 添加进数据库
        boolean save = userService.save(user1);

        return customMessageResult("注册成功", save);
    }
    @GetMapping("/getuserid")
    @ApiOperation("获取当前用户id")
    public Result getUserId(){
        String userId = userService.getUserId();
        return toDataResult(userId);
    }
    @ApiOperation("分页+搜索的查询显示")
    @PostMapping("/{currentPage}/{pageSize}")
    public Result pageAll(@PathVariable Integer currentPage,
                          @PathVariable Integer pageSize,
                          @RequestBody SelectUserDTO sud) {
        PageData pageData = userService.pageSelect(currentPage, pageSize, sud);
        System.out.println(pageData);
        return toDataResult(pageData);
    }


    @ApiOperation("用户详细信息增加")
    @PostMapping("/detail")
    public Result addDetail(@RequestBody User user) {
        return toResult(userService.saveOrUpdate(user));
    }

    @ApiOperation("用户账号注销")
    @DeleteMapping("/delete/{id}")
    public Result deleteUser(@PathVariable String id) {
        return toResult(userService.removeById(id));
    }

    @ApiOperation("用户账号注销")
    @DeleteMapping("/delete")
    public Result deleteAll(@RequestBody List<String> ids) {
        return toResult(userService.removeBatchByIds(ids));
    }

    @ApiOperation("id个人")
    @GetMapping("/{id}")
    public Result getOne(@PathVariable String id) {
        return toDataResult(userService.getById(id));
    }

    @ApiOperation("用户id查地址")
    @GetMapping("/address/{id}")
    public Result getAddressById(@PathVariable String id) {
        LambdaQueryWrapper<UserAddress> lam = new LambdaQueryWrapper<>();
        lam.eq(UserAddress::getUserId, id);
        return toDataResult(ias.list(lam));
    }
    @ApiOperation("添加或修改用户")
    @PostMapping("/addOrUpdata")
    public Result addOrUpdata(@RequestBody User user){
        return toDataResult(userService.saveOrUpdate(user));
    }

}

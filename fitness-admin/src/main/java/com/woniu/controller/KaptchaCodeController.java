package com.woniu.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.woniu.constant.RedisConstant;
import com.woniu.utils.Result;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;


@RestController
public class KaptchaCodeController {

    @Resource
    private DefaultKaptcha defaultKaptcha;

    @Resource
    private RedisTemplate redisTemplate;

    //生成验证码
    @RequestMapping("/code")
    public Result Code() throws IOException {
        // 生成文字验证码
        String text = defaultKaptcha.createText();
        System.out.println("文字验证码为"+text);

        // 将文字验证码保存到redis中，并设置有效期为3分钟
        redisTemplate.opsForValue().set(RedisConstant.LOGIN_AND_REGISTER_CODE,text,3, TimeUnit.MINUTES);

        // 生成图片验证码,并返回给前端
        ByteArrayOutputStream out = null;
        BufferedImage image = defaultKaptcha.createImage(text);
        out=new ByteArrayOutputStream();
        ImageIO.write(image,"jpg",out);

        // 对字节组Base64编码
        return new Result(200,"img", Base64.getEncoder().encodeToString(out.toByteArray()));
    }

}

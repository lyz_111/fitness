package com.woniu.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.vo.CommentVO;
import com.woniu.pojo.Comment;
import com.woniu.service.ICommentService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-10
 */
@RestController
@Api(tags = "评论控制器")
@RequestMapping("/comment")
public class CommentController extends BaseController {

    @Autowired
    private ICommentService ics;

    @ApiOperation("分页+搜索的查询显示")
    @PostMapping("/{currentPage}/{pageSize}")
    public Result pageSelectAll(@PathVariable Integer currentPage,
                          @PathVariable Integer pageSize,
                          @RequestParam(value = "category" ) String category) {
        return toDataResult(ics.selectPage(currentPage, pageSize,category));
    }

    @ApiOperation("分页全部的显示")
    @GetMapping("/{currentPage}/{pageSize}")
    public Result pageAll(@PathVariable Integer currentPage,
                          @PathVariable Integer pageSize) {
        return toDataResult(ics.allPage(currentPage, pageSize));
    }
    @ApiOperation("id删评论")
    @DeleteMapping("/{id}")
    public Result deltById(@PathVariable String id){
        return toResult(ics.removeById(id));
    }




}

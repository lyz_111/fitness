package com.woniu.controller;

import com.woniu.model.Mylog;
import com.woniu.model.dto.AddScheduleDTO;
import com.woniu.pojo.Schedule;
import com.woniu.service.IScheduleService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/schedule")
@Api(tags = "排班管理类")
public class ScheduleController extends BaseController {
    @Autowired
    private IScheduleService scheduleService;

    @GetMapping("/{currentPage}/{pageSize}")
    @ApiOperation("获取所有排班信息")
    @Mylog(value = "打开首页，查询所有排班")
    public Result getAllPageSchedule(@PathVariable Integer currentPage, @PathVariable Integer pageSize) {
        PageData schedule = scheduleService.getAllSchedule(currentPage, pageSize);
        return toDataResult(schedule);
    }
    @GetMapping("/get/{id}")
    @ApiOperation("根据id查询排班信息")
    @Mylog(value = "打开了编辑页面")
    public Result getScheduleById(@PathVariable String id){
        Schedule schedule = scheduleService.getById(id);
        return toDataResult(schedule);
    }
    @GetMapping("/select/{currentPage}/{pageSize}/{coachName}")
    @ApiOperation("根据教练名查询排班信息")
    @Mylog(value = "用教练名查询排班")
    public Result getScheduleByCoachName(@PathVariable String coachName,@PathVariable Integer currentPage, @PathVariable Integer pageSize){
        PageData schedule = scheduleService.getScheduleByCoachName(coachName, currentPage, pageSize);
        return toDataResult(schedule);
    }
    @DeleteMapping("/{id}")
    @ApiOperation("根据id删除排班")
    @Mylog("删除了排班信息")
    public Result deleteScheduleById(@PathVariable String id){
        boolean remove = scheduleService.deleteSchedule(id);
        return toDataResult(remove);
    }
    @PostMapping("/update")
    @ApiOperation("添加或修改排班")
    @Mylog("添加或修改了排班")
    public Result addOrUpdateSchedule(@RequestBody AddScheduleDTO addScheduleDTO){
        boolean b = scheduleService.addOrUpdateSchedule(addScheduleDTO);
        return toResult(b);
    }
}

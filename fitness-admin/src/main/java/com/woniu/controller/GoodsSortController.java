package com.woniu.controller;

import cn.hutool.core.bean.BeanUtil;
import com.woniu.model.dto.GoodsSortDTO;
import com.woniu.model.vo.GoodsSortVo;
import com.woniu.pojo.GoodsSort;
import com.woniu.service.IGoodsSortService;
import com.woniu.utils.PageData;
import com.woniu.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@RestController
@RequestMapping("/goodsSort")
@Api(tags = "商品分类")
public class GoodsSortController extends BaseController{
    @Resource
    private IGoodsSortService goodsSortService;
    @ApiOperation("分页查询")
    @PostMapping("/{pageNum}/{pageSize}")
    public Result queryGoodsSortApi(@PathVariable("pageNum") Integer pageNum,
                                  @PathVariable("pageSize") Integer pageSize,
                                  @RequestBody GoodsSortDTO goodsSortDTO){
        PageData pageData = goodsSortService.queryGoodsSort(pageNum,pageSize,goodsSortDTO);
        return toDataResult(pageData);
    }

    @ApiOperation("添加商品分类")
    @PostMapping
    public Result addGoodsSort(@RequestBody GoodsSortVo goodsSortVo){
        int flag = goodsSortService.addGoodsSort(goodsSortVo);
        return toResult(flag);
    }

    @ApiOperation("编辑商品分类")
    @PutMapping
    public Result updateGoodsSort(@RequestBody GoodsSortVo goodsSortVo){
        GoodsSort goodsSort = new GoodsSort();
        BeanUtils.copyProperties(goodsSortVo,goodsSort);
        boolean flag = goodsSortService.saveOrUpdate(goodsSort);
        return toResult(flag);
    }

    @ApiOperation("删除商品分类")
    @DeleteMapping("/{id}")
    public Result deleteGoodsSortApi(@PathVariable("id") String id){
        boolean flag = goodsSortService.removeById(id);
        return toResult(flag);
    }

    @ApiOperation("批量删除商品")
    @DeleteMapping("/")
    public Result deleteAllGoodsSort(@RequestBody List<String> ids){
        boolean flag = goodsSortService.removeBatchByIds(ids);
        return toResult(flag);
    }

    @ApiOperation("根据id查询商品分类")
    @GetMapping("/{id}")
    public Result queryGoodsSortId(@PathVariable("id") String id){
        GoodsSort goodsSort = goodsSortService.getById(id);
        return toDataResult(goodsSort);
    }
}

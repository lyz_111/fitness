package com.woniu;

import com.woniu.model.Mylog;
import com.woniu.pojo.Syslog;
import com.woniu.service.ISyslogService;
import com.woniu.service.IUserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Aspect
public class SyslogAspect {
    @Autowired
    private ISyslogService syslogService;
    @Autowired
    private IUserService userService;
    @Autowired
    private RedisTemplate redisTemplate;

    @Pointcut("@annotation(com.woniu.model.Mylog)")
    public void logPointCut() {
    }

    @AfterReturning("logPointCut()")
    public void saveSyslog(JoinPoint joinPoint) {
        Syslog syslog = new Syslog();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Mylog mylog = method.getAnnotation(Mylog.class);
        String methodName = method.getName();
        if (mylog != null) {
            String value = mylog.value();
            syslog.setOperation(value);
        }
        String className = joinPoint.getTarget().getClass().getName();
        syslog.setMethod(className + "." + methodName);
        String username = userService.getUsername();

        redisTemplate.opsForSet().add("persons", username);

        syslog.setUsername(username);
        syslogService.save(syslog);
    }
}

package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.pojo.Syslog;

public interface ISyslogService extends IService<Syslog> {
}

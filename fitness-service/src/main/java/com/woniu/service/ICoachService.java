package com.woniu.service;

import com.woniu.model.dto.SelectCoachDTO;
import com.woniu.pojo.Coach;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface ICoachService extends IService<Coach> {

    PageData pageSelect(Integer currentPage, Integer pageSize, SelectCoachDTO scd);

    //查询指定场馆教练
    List<Coach> getCoachByGymId(String gymId);

    //查询所有启用教练
    List<Coach> getCoachByIsOnline();

    //查询正常场馆下正常启用教练
    List<Coach> getCoach();

}

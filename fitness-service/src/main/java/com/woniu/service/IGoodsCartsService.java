package com.woniu.service;

import com.woniu.pojo.GoodsCarts;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGoodsCartsService extends IService<GoodsCarts> {

}

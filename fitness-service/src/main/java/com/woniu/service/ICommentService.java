package com.woniu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.vo.CommentVO;
import com.woniu.pojo.Comment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-10
 */
public interface ICommentService extends IService<Comment> {
    PageData allPage(Integer currentPage, Integer pageSize);

    PageData selectPage(Integer currentPage, Integer pageSize, String category);
}

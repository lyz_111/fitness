package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.GoodsSortDTO;
import com.woniu.model.vo.GoodsSortVo;
import com.woniu.pojo.GoodsSort;
import com.woniu.mapper.GoodsSortMapper;
import com.woniu.service.IGoodsSortService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GoodsSortServiceImpl extends ServiceImpl<GoodsSortMapper, GoodsSort> implements IGoodsSortService {
    @Resource
    private GoodsSortMapper mapper;
    @Override
    public PageData queryGoodsSort(Integer pageNum, Integer pageSize, GoodsSortDTO goodsSortDTO) {

        IPage<GoodsSort> page = new Page<>(pageNum,pageSize);
        LambdaQueryWrapper<GoodsSort> wrapper = new LambdaQueryWrapper(GoodsSort.class);
        if (wrapper != null){
            if (!StringUtils.isEmpty(goodsSortDTO.getSortName())){
                wrapper.like(GoodsSort::getSortName,goodsSortDTO.getSortName());
            }
        }
        page = mapper.selectPage(page,wrapper);
        PageData pageData = new PageData();
        pageData.setData(page.getRecords());
        pageData.setCurrentPage(page.getCurrent());
        pageData.setTotal(page.getTotal());
        return pageData;
    }

    @Override
    public int addGoodsSort(GoodsSortVo goodsSortVo) {
        GoodsSort goodsSort = new GoodsSort();

        BeanUtils.copyProperties(goodsSortVo,goodsSort);

        return mapper.insert(goodsSort);
    }

    @Override
    public int updateGoodsSort(GoodsSortVo goodsSortVo) {
        GoodsSort goodsSort = new GoodsSort();

        BeanUtils.copyProperties(goodsSortVo,goodsSort);

        return mapper.updateById(goodsSort);
    }

}

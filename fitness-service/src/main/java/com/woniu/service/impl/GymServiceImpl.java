package com.woniu.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.QueryGymDTO;
import com.woniu.pojo.Gym;
import com.woniu.mapper.GymMapper;
import com.woniu.pojo.GymDetail;
import com.woniu.service.IGymDetailService;
import com.woniu.service.IGymService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GymServiceImpl extends ServiceImpl<GymMapper, Gym> implements IGymService {

  @Autowired
  IGymDetailService gymDetailService;
  @Autowired
  GymMapper gymMapper;


  @Override
  public PageData findGymPage(Integer currentPage, Integer pageSize,
      QueryGymDTO queryGymDTO) {
    LambdaQueryWrapper<Gym> wrapper = Wrappers.lambdaQuery(Gym.class);
    if (queryGymDTO != null) {
      wrapper.like(!StringUtils.isEmpty(queryGymDTO.getName()), Gym::getName,
              queryGymDTO.getName())
          .like(!StringUtils.isEmpty(queryGymDTO.getAddress()), Gym::getAddress,
              queryGymDTO.getAddress())
          .eq(!StringUtils.isEmpty(queryGymDTO.getStatus()), Gym::getStatus,
              queryGymDTO.getStatus())
          .orderByAsc(Gym::getSort);
    }
    // 构建分页对象
    Page<Gym> page = new Page<>(currentPage, pageSize);
    // 查询
    Page<Gym> gymPage = gymMapper.selectPage(page, wrapper);
    // 返回数据
    return new PageData(gymPage.getTotal(), gymPage.getRecords());
  }

  @Override
  public boolean deleteGymById(String id) {
    //删除场馆
    boolean i;
    removeById(id);
    //删除场地
    LambdaQueryWrapper<GymDetail> qw = Wrappers.lambdaQuery(GymDetail.class);
    qw.eq(GymDetail::getGymId, id);
    i = gymDetailService.remove(qw);
    return i;
  }

    //查询所有正常使用场馆
    @Override
    public List<Gym> getGymByStatus() {
        LambdaQueryWrapper<Gym> wrapper = Wrappers.lambdaQuery(Gym.class);
        wrapper.eq(Gym::getStatus,1);
        List<Gym> gymList = this.list(wrapper);
        return gymList;
    }
}

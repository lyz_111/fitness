package com.woniu.service.impl;

import com.woniu.pojo.CourseOrderItems;
import com.woniu.mapper.CourseOrderItemsMapper;
import com.woniu.service.ICourseOrderItemsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class CourseOrderItemsServiceImpl extends ServiceImpl<CourseOrderItemsMapper, CourseOrderItems> implements ICourseOrderItemsService {

}

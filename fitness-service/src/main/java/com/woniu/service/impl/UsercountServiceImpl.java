package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.woniu.pojo.Usercount;
import com.woniu.mapper.UsercountMapper;
import com.woniu.service.IUsercountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-13
 */
@Service
public class UsercountServiceImpl extends ServiceImpl<UsercountMapper, Usercount> implements IUsercountService {
    @Override
    public List<Usercount> getAll() {
        QueryWrapper<Usercount> qw=new QueryWrapper<>();
        qw.orderByAsc("date");
        List<Usercount> list = this.list(qw);
        return list;
    }
}

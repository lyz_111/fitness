package com.woniu.service.impl;

import com.woniu.pojo.GoodsCarts;
import com.woniu.mapper.GoodsCartsMapper;
import com.woniu.service.IGoodsCartsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GoodsCartsServiceImpl extends ServiceImpl<GoodsCartsMapper, GoodsCarts> implements IGoodsCartsService {

}

package com.woniu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.mapper.SyslogMapper;
import com.woniu.pojo.Syslog;
import com.woniu.service.ISyslogService;
import org.springframework.stereotype.Service;

@Service
public class SyslogServiceImpl extends ServiceImpl<SyslogMapper, Syslog> implements ISyslogService {
}

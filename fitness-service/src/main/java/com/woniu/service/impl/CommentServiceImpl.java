package com.woniu.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.vo.CommentVO;
import com.woniu.pojo.Comment;
import com.woniu.mapper.CommentMapper;
import com.woniu.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-10
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {
@Autowired
private CommentMapper ICS;
    @Override
    public PageData allPage(Integer currentPage, Integer pageSize) {
        // 构建分页对象
        Page<CommentVO> page = new Page<>(currentPage, pageSize);
        Page<CommentVO> page1 = ICS.selectVoPage(page);
        // 返回数据
        return new PageData(page1.getTotal(), page1.getRecords());
    }

    @Override
    public PageData selectPage(Integer currentPage, Integer pageSize, String category) {
        Page<CommentVO> page = new Page<>(currentPage, pageSize);
        Page<CommentVO> page1 = ICS.selectVoByCategory(page,category);
        // 返回数据
        return new PageData(page1.getTotal(), page1.getRecords());
    }
}

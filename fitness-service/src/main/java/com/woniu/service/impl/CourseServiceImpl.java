package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.QueryCourseDTO;
import com.woniu.pojo.Course;
import com.woniu.mapper.CourseMapper;
import com.woniu.service.ICourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    //分页查询课程、根据课程名称及教练名称模糊查寻课程
    @Override
    public PageData getCoursePage(Integer currentPage, Integer pageSize, QueryCourseDTO queryCourseDTO) {
        //查询条件
        LambdaQueryWrapper<Course> wrapper = Wrappers.lambdaQuery(Course.class);
        if (!StringUtils.isEmpty(queryCourseDTO.getCourseName())){
            wrapper.like(Course::getName,queryCourseDTO.getCourseName());
        }
        if (!StringUtils.isEmpty(queryCourseDTO.getCoachId())){
            wrapper.eq(Course::getCoachId,queryCourseDTO.getCoachId());
        }
        //获取ipage
        IPage<Course> iPage = new Page<>(currentPage,pageSize);
        //调用分页方法
        IPage<Course> courseIPage = this.page(iPage, wrapper);
        //封装分页结果
        return new PageData(courseIPage.getTotal(),courseIPage.getRecords());
    }

    //添加、修改课程
    @Override
    public String saveOrUpdateCourse(Course course) {

        boolean flag = this.saveOrUpdate(course);

        if (flag){
            return course.getId();
        }

        return null;
    }

}

package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.GoodsAddOrUpdateDTO;
import com.woniu.model.dto.GoodsDTO;
import com.woniu.pojo.Goods;
import com.woniu.mapper.GoodsMapper;
import com.woniu.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {
    @Resource
    private GoodsMapper mapper;

    @Override
    public PageData queryGoodsPage(Integer pageNum, Integer pageSize, GoodsDTO goodsDTO) {
        IPage<Goods> page = new Page<>(pageNum,pageSize);
        // 调用自定义的查询方法
        List<GoodsDTO> goodsList = mapper.queryGoodsPage(page,goodsDTO);
        //自然定制排序
        Collections.sort(goodsList) ;

        //BeanUtils.copyProperties(goodsList, goodsDTO);
        // 构建 PageData 对象
        PageData pageData = new PageData();
        pageData.setTotal(page.getTotal());
        pageData.setCurrentPage(page.getCurrent());
        pageData.setData(goodsList);
        return pageData;
    }

    @Override
    public int addGoods(GoodsAddOrUpdateDTO goodsAddDTO) {
        //取出总记录，然后+1
        Integer numbers = mapper.getTotalNumbers();

        Goods goods = new Goods();
        goods.setNumbers(++numbers);
        goods.setSales(0);
        BeanUtils.copyProperties(goodsAddDTO,goods);

        return mapper.insert(goods);
    }

    @Override
    public int updateGoods(GoodsAddOrUpdateDTO goodsUpdateDTO) {
        Goods goods = new Goods();

        BeanUtils.copyProperties(goodsUpdateDTO,goods);
        return mapper.updateById(goods);
    }

    @Override
    public int queryOnline() {
        return mapper.queryOnline();
    }

    @Override
    public int queryNoOnline() {
        return mapper.queryNoOnline();
    }

}

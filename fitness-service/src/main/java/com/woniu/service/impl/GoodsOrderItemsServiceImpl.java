package com.woniu.service.impl;

import com.woniu.pojo.GoodsOrderItems;
import com.woniu.mapper.GoodsOrderItemsMapper;
import com.woniu.service.IGoodsOrderItemsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GoodsOrderItemsServiceImpl extends ServiceImpl<GoodsOrderItemsMapper, GoodsOrderItems> implements IGoodsOrderItemsService {

}

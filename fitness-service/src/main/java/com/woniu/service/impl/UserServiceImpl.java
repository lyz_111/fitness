package com.woniu.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.SelectUserDTO;
import com.woniu.model.vo.CoachVo;
import com.woniu.model.vo.UserVO;
import com.woniu.pojo.User;
import com.woniu.mapper.UserMapper;
import com.woniu.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {



    @Resource
    private UserMapper userMapper;

    @Override
    public boolean login(String username, String password) {

        //使用hutool的Bcrypt工具类解析
        //拿用户输入的密码 与 数据库中的加密密码 利用解析工具 验证
        String pwd = userMapper.selectPwdByUsername(username);

        return BCrypt.checkpw(password,pwd);
    }

    @Override
    public String getPhotoByUsername(String username) {
        return userMapper.getPhotoByUsername(username);
    }

    @Override
    public String getUsername() {
        //根据springMvc的RequestContextHolder获取当前线程的request对象
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = servletRequestAttributes.getRequest();

        //获取token
        String token = request.getHeader("token");
        //获取token中保存的username
        JWT jwt = JWTUtil.parseToken(token);
        String username = (String)jwt.getPayload("username");

        return username;
    }

    @Override
    public String getUserId() {
        return userMapper.getUserIdByUsername(getUsername());
    }

    @Override
    public User getUserByUserName(String username) {
        return userMapper.getUserByUserName(username);
    }

    @Override
    public PageData pageSelect(Integer currentPage, Integer pageSize, SelectUserDTO sud) {
        // 构建分页对象
        Page<UserVO> page = new Page<>(currentPage, pageSize);
        Page<UserVO> page1 = userMapper.selectVoPage(page, sud);
        return new PageData(page1.getTotal(),page1.getRecords());
    }
}

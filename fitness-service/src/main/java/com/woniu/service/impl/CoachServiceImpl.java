package com.woniu.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.mapper.CoachMapper;
import com.woniu.mapper.GymMapper;
import com.woniu.model.dto.SelectCoachDTO;
import com.woniu.model.vo.CoachVo;
import com.woniu.pojo.Coach;
import com.woniu.pojo.Gym;
import com.woniu.service.ICoachService;
import com.woniu.service.IGymService;
import com.woniu.utils.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class CoachServiceImpl extends ServiceImpl<CoachMapper, Coach> implements ICoachService {

    @Autowired(required = false)
    private   CoachMapper cm;

    @Autowired(required = false)
    private IGymService gymService;

    @Override
    public PageData pageSelect(Integer currentPage, Integer pageSize, SelectCoachDTO scd) {


        // 构建分页对象
        Page<CoachVo> page = new Page<>(currentPage, pageSize);
        Page<CoachVo> page1 = cm.selectVoPage(page, scd);
        // 返回数据
        return new PageData(page1.getTotal(), page1.getRecords());

    }

    //查询指定场馆教练
    @Override
    public List<Coach> getCoachByGymId(String gymId) {
        //根据gymId查询教练、选择被启用的教练
        LambdaQueryWrapper<Coach> wrapper = Wrappers.lambdaQuery(Coach.class);
        wrapper.eq(Coach::getGymId,gymId);
        wrapper.eq(Coach::getIsOnline,1);
        List<Coach> coachList = this.list(wrapper);
        return coachList;
    }

    //查询所有启用教练
    @Override
    public List<Coach> getCoachByIsOnline() {
        //查询所有启用教练条件，isOnline为1即启用
        LambdaQueryWrapper<Coach> wrapper = Wrappers.lambdaQuery(Coach.class);
        wrapper.eq(Coach::getIsOnline,1);
        List<Coach> coachList = this.list(wrapper);
        return coachList;
    }

    //查询正常场馆下正常启用教练
    @Override
    public List<Coach> getCoach() {
        List<Coach> coaches = new ArrayList<>();
        List<Coach> coachList = this.getCoachByIsOnline();
        for (Coach coach : coachList) {
            Gym gym = gymService.getById(coach.getGymId());
            if (Integer.parseInt(gym.getStatus()) == 1){
                coaches.add(coach);
            }
        }
        return coaches;
    }

}


package com.woniu.service.impl;

import com.woniu.pojo.Appointment;
import com.woniu.mapper.AppointmentMapper;
import com.woniu.service.IAppointmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class AppointmentServiceImpl extends ServiceImpl<AppointmentMapper, Appointment> implements IAppointmentService {

}

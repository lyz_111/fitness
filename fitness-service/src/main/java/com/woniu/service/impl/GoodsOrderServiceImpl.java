package com.woniu.service.impl;

import com.woniu.pojo.GoodsOrder;
import com.woniu.mapper.GoodsOrderMapper;
import com.woniu.service.IGoodsOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GoodsOrderServiceImpl extends ServiceImpl<GoodsOrderMapper, GoodsOrder> implements IGoodsOrderService {

}

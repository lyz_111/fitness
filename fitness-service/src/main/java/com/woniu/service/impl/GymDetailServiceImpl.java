package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.woniu.pojo.Gym;
import com.woniu.pojo.GymDetail;
import com.woniu.mapper.GymDetailMapper;
import com.woniu.service.IGymDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class GymDetailServiceImpl extends ServiceImpl<GymDetailMapper, GymDetail> implements
    IGymDetailService {


  @Override
  public List<GymDetail> findGymPlaceByGymId(String gymId) {
    LambdaQueryWrapper<GymDetail> qw = Wrappers.lambdaQuery(
        GymDetail.class);
    qw.eq(GymDetail::getGymId, gymId);
    List<GymDetail> list = list(qw);
    return list;
  }
}

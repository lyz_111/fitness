package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.AddScheduleDTO;
import com.woniu.model.vo.GetAllScheduleVO;
import com.woniu.pojo.Appointment;
import com.woniu.pojo.Schedule;
import com.woniu.mapper.ScheduleMapper;
import com.woniu.service.IAppointmentService;
import com.woniu.service.IScheduleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.utils.PageData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleMapper, Schedule> implements IScheduleService {
    @Autowired
    private ScheduleMapper scheduleMapper;
    @Autowired
    private IAppointmentService appointmentService;

    @Override
    public PageData getAllSchedule(Integer currentPage, Integer pageSize) {
        IPage<GetAllScheduleVO> iPage = new Page<>(currentPage, pageSize);
        IPage<GetAllScheduleVO> allSchedule = scheduleMapper.getAllSchedule(iPage);
        return new PageData(allSchedule.getTotal(), allSchedule.getRecords());
    }

    @Override
    public PageData getScheduleByCoachName(String coachName, Integer currentPage, Integer pageSize) {
        if (StringUtils.isEmpty(coachName)) {
            return this.getAllSchedule(currentPage, pageSize);
        }
        IPage<GetAllScheduleVO> iPage = new Page<>(currentPage, pageSize);
        IPage<GetAllScheduleVO> scheduleByCoachName = scheduleMapper.getScheduleByCoachName(iPage, coachName);
        return new PageData(scheduleByCoachName.getTotal(), scheduleByCoachName.getRecords());
    }

    @Override
    @Transactional
    public boolean deleteSchedule(String id) {
        QueryWrapper<Appointment> qw = new QueryWrapper<>();
        qw.eq("schedule_id", id);
        this.removeById(id);
        boolean remove = appointmentService.remove(qw);
        return remove;
    }

    @Override
    @Transactional
    public boolean addOrUpdateSchedule(AddScheduleDTO addScheduleDTO) {
        if(StringUtils.isEmpty(addScheduleDTO.getId())){
            Schedule schedule=new Schedule();
            BeanUtils.copyProperties(addScheduleDTO,schedule);
            //手动生成雪花id，保证两表id的一致性
            String id = String.valueOf(IdWorker.getId());
            schedule.setId(id);
            this.save(schedule);
            Appointment appointment=new Appointment();
            appointment.setCourseId(addScheduleDTO.getCourseId());
            appointment.setUserId(addScheduleDTO.getUserId());
            appointment.setGymId(addScheduleDTO.getGymId());
            appointment.setScheduleId(id);
            boolean save = appointmentService.save(appointment);
            return save;
        }
        Schedule schedule1=new Schedule();
        BeanUtils.copyProperties(addScheduleDTO,schedule1);
        this.updateById(schedule1);
        String scheduleId = this.getById(addScheduleDTO.getId()).getId();
        QueryWrapper<Appointment> qw=new QueryWrapper<>();
        qw.eq("schedule_id",scheduleId);
        Appointment appointment1 = appointmentService.getOne(qw);
        appointment1.setCourseId(addScheduleDTO.getCourseId());
        appointment1.setUserId(addScheduleDTO.getUserId());
        appointment1.setGymId(addScheduleDTO.getGymId());
        appointment1.setUpdateTime(new Date());
        boolean update = appointmentService.updateById(appointment1);
        return update;
    }
}

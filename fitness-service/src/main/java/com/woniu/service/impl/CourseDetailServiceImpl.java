package com.woniu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.woniu.mapper.CourseDetailMapper;
import com.woniu.pojo.CourseDetail;
import com.woniu.service.ICourseDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author group_one
 * @since 2023-07-08
 */
@Service
public class CourseDetailServiceImpl extends ServiceImpl<CourseDetailMapper, CourseDetail> implements ICourseDetailService {

    //通过courseId查询课程细节描述
    @Override
    public CourseDetail getCourseDetailByCourseId(String courseId) {
        //通过courseId查询课程细节描述
        LambdaQueryWrapper<CourseDetail> wrapper = Wrappers.lambdaQuery(CourseDetail.class);
        wrapper.eq(CourseDetail::getCourseId,courseId);
        CourseDetail courseDetail = this.getOne(wrapper);
        return courseDetail;
    }

    //添加、修改课程描述（返回courseId）
    @Override
    public String saveOrUpdateCourseDetail(CourseDetail courseDetail) {
        boolean flag = this.saveOrUpdate(courseDetail);
        if (flag){
            return courseDetail.getCourseId();
        }
        return null;
    }
}

package com.woniu.service;

import com.woniu.pojo.GymDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGymDetailService extends IService<GymDetail> {
  List<GymDetail> findGymPlaceByGymId(String gymId);
}

package com.woniu.service;

import com.woniu.model.dto.QueryCourseDTO;
import com.woniu.pojo.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface ICourseService extends IService<Course> {

    //分页查询课程、根据教练名称及课程名称模糊查寻课程
    PageData getCoursePage(Integer currentPage, Integer pageSize, QueryCourseDTO queryCourseDTO);

    //添加、修改课程
    String saveOrUpdateCourse(Course course);
}

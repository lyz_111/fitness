package com.woniu.service;

import com.woniu.model.dto.SelectUserDTO;
import com.woniu.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IUserService extends IService<User> {

    /**
     * 用户根据用户名与密码实现登录
     * @param username 用户名
     * @param password 登录密码
     * @return
     */
     boolean login(String username, String password);

    /**
     * 根据用户名获取用户的头像
     * @param username 用户名
     * @return
     */
    String getPhotoByUsername(String username);


    /**
     * 提供给其他模块的接口，返回用户名
     * @return
     */
    String getUsername();


    /**
     * 提供给其他模块的接口，返回用户id
     * @return
     */
    String getUserId();


    /**
     * 根据用户名查询用户
     * @param username 用户名
     * @return
     */
    User getUserByUserName(String username);

    PageData pageSelect(Integer currentPage, Integer pageSize, SelectUserDTO sud);
}

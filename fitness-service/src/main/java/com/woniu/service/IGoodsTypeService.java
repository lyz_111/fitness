package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.pojo.GoodsType;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGoodsTypeService extends IService<GoodsType> {

}

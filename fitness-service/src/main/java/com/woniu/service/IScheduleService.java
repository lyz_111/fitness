package com.woniu.service;

import com.woniu.model.dto.AddScheduleDTO;
import com.woniu.pojo.Schedule;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IScheduleService extends IService<Schedule> {
    PageData getAllSchedule(Integer currentPage, Integer pageSize);
    PageData getScheduleByCoachName(String coachName,Integer currentPage, Integer pageSize);
    boolean deleteSchedule(String id);
    boolean addOrUpdateSchedule(AddScheduleDTO addScheduleDTO);
}

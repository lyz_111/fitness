package com.woniu.service;

import com.woniu.pojo.Usercount;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-13
 */
public interface IUsercountService extends IService<Usercount> {
    List<Usercount> getAll();
}

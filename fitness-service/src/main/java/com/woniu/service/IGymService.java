package com.woniu.service;


import com.woniu.model.dto.QueryGymDTO;
import com.woniu.pojo.Gym;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

import java.util.List;


/**
 * <p>
 * 服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGymService extends IService<Gym> {

  PageData findGymPage(Integer currentPage, Integer pageSize, QueryGymDTO queryGymDTO);

  boolean deleteGymById(String id);

  //查询所有正常使用场馆
  List<Gym> getGymByStatus();

}

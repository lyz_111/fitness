package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.pojo.CourseDetail;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-08
 */
public interface ICourseDetailService extends IService<CourseDetail> {

    //通过courseId查询课程细节描述
    CourseDetail getCourseDetailByCourseId(String courseId);

    //添加、修改课程描述（返回courseId）
    String saveOrUpdateCourseDetail(CourseDetail courseDetail);

}

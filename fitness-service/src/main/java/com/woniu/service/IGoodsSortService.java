package com.woniu.service;

import com.woniu.model.dto.GoodsSortDTO;
import com.woniu.model.vo.GoodsSortVo;
import com.woniu.pojo.GoodsSort;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGoodsSortService extends IService<GoodsSort> {

    PageData queryGoodsSort(Integer pageNum, Integer pageSize, GoodsSortDTO goodsSortDTO);

    int addGoodsSort(GoodsSortVo goodsSortVo);

    int updateGoodsSort(GoodsSortVo goodsSortVo);
}

package com.woniu.service;

import com.woniu.model.dto.GoodsAddOrUpdateDTO;
import com.woniu.model.dto.GoodsDTO;
import com.woniu.pojo.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.utils.PageData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface IGoodsService extends IService<Goods> {

    PageData queryGoodsPage(Integer pageNum, Integer pageSize, GoodsDTO goodsDTO);

    int addGoods(GoodsAddOrUpdateDTO goodsAddDTO);

    int updateGoods(GoodsAddOrUpdateDTO goodsUpdateDTO);

    int queryOnline();

    int queryNoOnline();
}

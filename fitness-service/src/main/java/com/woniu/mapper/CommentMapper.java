package com.woniu.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.vo.CommentVO;
import com.woniu.pojo.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-10
 */
public interface CommentMapper extends BaseMapper<Comment> {
    Page<CommentVO> selectVoPage(Page<CommentVO> page);

    Page<CommentVO> selectVoByCategory(Page<CommentVO> page, String category);
}

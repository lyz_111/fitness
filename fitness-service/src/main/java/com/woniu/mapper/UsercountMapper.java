package com.woniu.mapper;

import com.woniu.pojo.Usercount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-13
 */
public interface UsercountMapper extends BaseMapper<Usercount> {

}

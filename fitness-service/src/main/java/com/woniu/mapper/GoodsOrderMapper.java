package com.woniu.mapper;

import com.woniu.pojo.GoodsOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface GoodsOrderMapper extends BaseMapper<GoodsOrder> {

}

package com.woniu.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.woniu.model.dto.GoodsDTO;
import com.woniu.pojo.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    List<GoodsDTO> queryGoodsPage(IPage page, @Param("goodsDTO") GoodsDTO goodsDTO);

    @Select("select max(numbers) from goods")
    Integer getTotalNumbers();
    @Select("select count(is_online) from goods where is_online = 1")
    int queryOnline();
    @Select("select count(is_online) from goods where is_online = 2")
    int queryNoOnline();
}

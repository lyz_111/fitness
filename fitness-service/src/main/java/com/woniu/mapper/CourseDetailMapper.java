package com.woniu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.pojo.CourseDetail;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-08
 */
public interface CourseDetailMapper extends BaseMapper<CourseDetail> {

}

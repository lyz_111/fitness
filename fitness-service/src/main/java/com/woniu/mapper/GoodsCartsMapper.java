package com.woniu.mapper;

import com.woniu.pojo.GoodsCarts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface GoodsCartsMapper extends BaseMapper<GoodsCarts> {

}

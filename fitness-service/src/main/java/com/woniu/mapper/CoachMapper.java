package com.woniu.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.SelectCoachDTO;
import com.woniu.model.vo.CoachVo;
import com.woniu.pojo.Coach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface CoachMapper extends BaseMapper<Coach> {

    Page<CoachVo> selectVoPage(IPage<CoachVo> page,@Param("selectCoachDTO") SelectCoachDTO selectCoachDTO);
}


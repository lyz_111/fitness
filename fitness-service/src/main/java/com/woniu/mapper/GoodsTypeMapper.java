package com.woniu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.model.dto.GoodsDTO;
import com.woniu.pojo.Goods;
import com.woniu.pojo.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface GoodsTypeMapper extends BaseMapper<GoodsType> {
}

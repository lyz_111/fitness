package com.woniu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.pojo.Syslog;

public interface SyslogMapper extends BaseMapper<Syslog> {
}

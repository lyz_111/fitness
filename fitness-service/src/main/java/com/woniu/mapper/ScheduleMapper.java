package com.woniu.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.woniu.model.vo.GetAllScheduleVO;
import com.woniu.pojo.Schedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface ScheduleMapper extends BaseMapper<Schedule> {
    IPage<GetAllScheduleVO> getAllSchedule(IPage<GetAllScheduleVO> page);
    IPage<GetAllScheduleVO> getScheduleByCoachName(IPage<GetAllScheduleVO> page,String coachName);

}

package com.woniu.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.woniu.model.dto.SelectUserDTO;
import com.woniu.model.vo.UserVO;
import com.woniu.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface UserMapper extends BaseMapper<User> {

    @Select("select password from user where username = #{username}")
    String selectPwdByUsername(String username);

    @Select("select photo from user where username = #{username}")
    String getPhotoByUsername(String username);

    @Select("select id from user where username = #{username}")
    String getUserIdByUsername(String username);

    @Select("select * from user where username = #{username}")
    User getUserByUserName(String username);

    Page<UserVO> selectVoPage(Page<UserVO> page, @Param("selectUserDTO")SelectUserDTO sud);
}

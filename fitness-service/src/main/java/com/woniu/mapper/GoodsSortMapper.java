package com.woniu.mapper;

import com.woniu.pojo.GoodsSort;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author group_one
 * @since 2023-07-04
 */
public interface GoodsSortMapper extends BaseMapper<GoodsSort> {
    @Select("select gs.sort_name from goods_sort gs,goods g where g.goods_sort_id = gs.id and g.is_delete = 0")
    void queryName();
}

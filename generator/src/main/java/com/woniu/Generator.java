package com.woniu;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;

public class Generator {

    public static void main(String[] args) {

        //自己更改
        String path = "D:\\JavaSEWork\\fitness\\generator\\src\\main\\";

        FastAutoGenerator.create("jdbc:mysql://175.178.159.70:3306/fitness?createDatabaseIfNotExist=true&useSSL=false",
                "remote_user", "root")
                .globalConfig(builder -> {
                    builder.author("group_one") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .dateType(DateType.ONLY_DATE) //时间设置
                            .outputDir(path + "java"); // 指定输出目录
                })

                .packageConfig(builder -> {
                    builder.parent("com.woniu") // 设置父包名
                            .moduleName(null) // 设置父包模块名
                            .entity("pojo") // 设置实体类的包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, path + "resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    //自己更改
                    builder.addInclude("usercount") // 设置需要生成的表名
                            .addTablePrefix(""); // 设置过滤表前缀
                    builder.controllerBuilder().enableRestStyle();// 设置restController
                    builder.entityBuilder().enableLombok();
                })
                .execute();
    }
}
